"""
Given an array arr of integers, check if there exists two integers N and M such that N is the double of M ( i.e. N = 2 * M).

More formally check if there exists two indices i and j such that :

i != j
0 <= i, j < arr.length
arr[i] == 2 * arr[j]
 

Example 1:

Input: arr = [10,2,5,3]
Output: true
Explanation: N = 10 is the double of M = 5,that is, 10 = 2 * 5.
Example 2:

Input: arr = [7,1,14,11]
Output: true
Explanation: N = 14 is the double of M = 7,that is, 14 = 2 * 7.
Example 3:

Input: arr = [3,1,7,11]
Output: false
Explanation: In this case does not exist N and M, such that N = 2 * M.
 

Constraints:

2 <= arr.length <= 500
-10^3 <= arr[i] <= 10^3
"""

# Code
class Solution:
    def checkIfExist(self, arr: List[int]) -> bool:
        arr_sort = sorted(arr)
        
        # count = len(arr_sort)//2
        # print("Sorted arr is ", arr_sort)
        for _ in range(len(arr_sort)):
            
            # print(_)
            target = arr_sort[_]*2
            # print("Target is ", target)
            
            start = 0
            end = len(arr_sort)
        
            if ((target > arr_sort[-1]) or (target < arr_sort[0])):
                continue

            while(start <= end):
                mid = ((end - start)//2) + start

                if ((target == arr_sort[mid]) and (mid != _)):
                    return(True)
                elif(target > arr_sort[mid]):
                    start = mid + 1
                else:
                    end = mid - 1

        
        return(False)
            


"""

107 / 107 test cases passed.
Status: Accepted
Runtime: 52 ms
Memory Usage: 14.5 MB
"""
