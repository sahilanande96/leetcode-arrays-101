"""
Given an array nums of integers, return how many of them contain an even number of digits.
 

Example 1:

Input: nums = [12,345,2,6,7896]
Output: 2
Explanation: 
12 contains 2 digits (even number of digits). 
345 contains 3 digits (odd number of digits). 
2 contains 1 digit (odd number of digits). 
6 contains 1 digit (odd number of digits). 
7896 contains 4 digits (even number of digits). 
Therefore only 12 and 7896 contain an even number of digits.
Example 2:

Input: nums = [555,901,482,1771]
Output: 1 
Explanation: 
Only 1771 contains an even number of digits.
 

Constraints:

1 <= nums.length <= 500
1 <= nums[i] <= 10^5

"""


# Code
# This uses 2 for loops
# class Solution:
#     def findNumbers(self, nums: List[int]) -> int:
#         even = 0
#         count = 0
#         for _ in range(len(nums)):
#             for j in str(nums[_]):
#                 count += 1
#             if count % 2 == 0:
#                 even += 1
#             count = 0
#         return(even)


# This contains a single for loop
class Solution:
    def findNumbers(self, nums: List[int]) -> int:
        even = 0
        count = 0
        for _ in nums:
            len_string_num = len(str(_))
            if len_string_num % 2 == 0:
                even += 1
        return(even)
                
                

"""
104 / 104 test cases passed.
Status: Accepted
Runtime: 56 ms
Memory Usage: 14.4 MB
"""
