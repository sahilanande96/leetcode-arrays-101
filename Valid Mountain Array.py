"""
Given an array of integers arr, return true if and only if it is a valid mountain array.

Recall that arr is a mountain array if and only if:

arr.length >= 3
There exists some i with 0 < i < arr.length - 1 such that:
arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
arr[i] > arr[i + 1] > ... > arr[arr.length - 1]

 

Example 1:

Input: arr = [2,1]
Output: false
Example 2:

Input: arr = [3,5,5]
Output: false
Example 3:

Input: arr = [0,3,2,1]
Output: true
 

Constraints:

1 <= arr.length <= 104
0 <= arr[i] <= 104
"""

# Code
class Solution:
    def validMountainArray(self, arr: List[int]) -> bool:
        if len(arr) < 3:
            return(False)
        
        rising, falling = 0, 0
        for _ in range(1, len(arr)):
            if arr[_ - 1] == arr[_]:
                return(False)
            elif arr[_ - 1] < arr[_]:
                rising += 1
                if rising > 0 and falling > 0:
                    return(False)
            else:
                falling += 1
            
        if (falling == 0 or rising == 0):
            return(False)
        return(True)
                


"""
53 / 53 test cases passed.
Status: Accepted
Runtime: 212 ms
Memory Usage: 15.7 MB
"""
