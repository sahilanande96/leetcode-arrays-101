"""
Given a binary array, find the maximum number of consecutive 1s in this array.

Example 1:
Input: [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s.
    The maximum number of consecutive 1s is 3.
Note:

The input array will only contain 0 and 1.
The length of input array is a positive integer and will not exceed 10,000

"""

# Code
class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        total_len = len(nums)
        start = 0
        count = 0
        while(1):
            try:
                occ = nums.index(0,start, total_len)
                
                if (count <= (occ-start)):
                    count = occ - start

                start = occ + 1

            except:
                if (count == 0 and start == total_len):
                    return(0)
                elif start <= total_len:
                    if count > (total_len-start):
                        return(count)
                    else:
                        return(total_len-start)
            
        
"""
41 / 41 test cases passed.
Status: Accepted
Runtime: 332 ms
Memory Usage: 14.5 MB
"""
